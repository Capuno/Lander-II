# Disclaimer

All songs here are licensed under Creative Commons 1.0 Universal, this is the list of authors:

Music:

* almusic_other-world.ogg - Author: Almusic34 https://freesound.org/people/almusic34/ Title: Other World 4
* greek555_pulsations.ogg - Author: Greek555 https://freesound.org/people/Greek555/ Title: Pulsations One Hundred
* electronic_minute81.ogg - Author: gis_sweden https://freesound.org/people/gis_sweden/ Title: Electronic Minute No 81 - Moving Sequence

Sounds:

* Menu Select Button https://freesound.org/people/modularsamples/sounds/302826/ (btnSelectMenu.ogg)
* AFK Blur and Focus https://freesound.org/people/ellenmentor/sounds/419161/ (ooh.ogg, aah.ogg)
* Ship Laser Normal https://freesound.org/people/Julien%20Matthey/sounds/268343/ (laser.ogg)