#include <stdlib.h>
#include <vector>
#include <map>
#include "entity.hpp"

#define STAGE_EASY 1000
#define STAGE_MEDIUM 2000
#define STAGE_HARD 3000
#define STAGE_BOSS 4000
#define STAGE_LEET 5000
#define STAGE_TAS_ONLY 6000

struct panel_s {

    unsigned int baseWidth, baseHeight;
    std::vector<sf::RectangleShape> panels;

    void load(uint16_t basePos, uint16_t w, uint16_t h) {

        uint16_t bh = basePos + h/2 - h/4/2; // I believe g++ changes this to mul so I don't care
        this->baseWidth = w/3;
        this->baseHeight = h/4;

        sf::RectangleShape rBase(sf::Vector2f(w,h));
        rBase.setPosition(0, basePos);
        rBase.setFillColor(sf::Color(bg.r,bg.g,bg.b));
        this->panels.push_back(rBase);

        sf::RectangleShape pBase(sf::Vector2f(w,h));
        pBase.setPosition(0, basePos);
        pBase.setFillColor(sf::Color(panelBase.r, panelBase.g, panelBase.b, panelBase.a));
        this->panels.push_back(pBase);

        sf::RectangleShape pHPbase(sf::Vector2f(this->baseWidth, this->baseHeight));
        pHPbase.setPosition(w/9, bh);
        pHPbase.setFillColor(sf::Color(panelEmpty.r, panelEmpty.g, panelEmpty.b, panelEmpty.a));
        this->panels.push_back(pHPbase);

        sf::RectangleShape pPPbase(sf::Vector2f(this->baseWidth, this->baseHeight));
        pPPbase.setPosition(w/9*5, bh);
        pPPbase.setFillColor(sf::Color(panelEmpty.r, panelEmpty.g, panelEmpty.b, panelEmpty.a));
        this->panels.push_back(pPPbase);

        sf::RectangleShape pPH(sf::Vector2f(this->baseWidth, this->baseHeight));
        pPH.setPosition(w/9, bh);
        pPH.setFillColor(sf::Color(panelFill.r, panelFill.g, panelFill.b, panelFill.a));
        this->panels.push_back(pPH);

        sf::RectangleShape pPP(sf::Vector2f(this->baseWidth, this->baseHeight));
        pPP.setPosition(w/9*5, bh);
        pPP.setFillColor(sf::Color(panelFill.r, panelFill.g, panelFill.b, panelFill.a));
        this->panels.push_back(pPP);

    }

    void update(uint8_t HP, uint16_t PP) {
        this->panels[4].setSize(sf::Vector2f(baseWidth*(HP/100.f), baseHeight));
        this->panels[5].setSize(sf::Vector2f(baseWidth*(PP/1000.f), baseHeight));
    }

};

class world {

    private:

        uint8_t speedBonus;
        bool afk_b, afk_h;
        uint16_t worldHeight;
        unsigned int phase;
        sf::Rect<float> starPosBuffer;
        sf::VideoMode desktop;
        sf::RenderWindow window;
        sf::Font fontShip, fontStar;
        sf::Music startMusic, bossEasyMusic;
        std::wstring ascii_ship, ascii_enemy_easy;
        ship_c *ship;
        panel_s panel;
        std::vector<bullet_c> bullets;
        std::vector<sf::Text> stars;
        std::vector<enemy_easy_c> enemies_easy;
        sf::Text dbg;
        std::map<const char*, sf::Sound> soundBoard;

        void gameLoop() {

            while (window.isOpen()) {

                if (ship->position.x <= 0 || ship->position.x >= desktop.width - ship->bounds.width) {
                    ship->speed.x *= .66;
                    ship->speed.x = -ship->speed.x;
                    ship->position.x = ship->position.x <= 0 ? 0 : desktop.width - ship->bounds.width;
                }

                if (ship->position.y <= 0 || ship->position.y >= worldHeight - ship->bounds.height) {
                    ship->speed.y *= .66;
                    ship->speed.y = -ship->speed.y;
                    ship->position.y = ship->position.y <= 0 ? 0 : worldHeight - ship->bounds.height;
                }

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
                    ship->speed.x > 0 ? ship->speed.x-- : ( ship->speed.x == 0 ?: ship->speed.x++ );
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
                    ship->speed.x <= -shipMaxSpeed ?: ship->speed.x -= shipAcceleration;
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
                    ship->speed.x >= shipMaxSpeed ?: ship->speed.x += shipAcceleration;
                } else {
                    ship->speed.x > 0 ? ship->speed.x-- : ( ship->speed.x == 0 ?: ship->speed.x++ );
                }

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
                    ship->speed.y > 0 ? ship->speed.y-- : ( ship->speed.y == 0 ?: ship->speed.y++ );
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
                    ship->speed.y <= -shipMaxSpeed ?: ship->speed.y -= shipAcceleration;
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
                    ship->speed.y >= shipMaxSpeed ?: ship->speed.y += shipAcceleration;
                } else {
                    ship->speed.y > 0 ? ship->speed.y-- : ( ship->speed.y == 0 ?: ship->speed.y++ );
                }

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::X)) {
                    if (ship->canShoot) {
                        for (int i = 0; i < totalGuns; ++i) {
                            bullet_c b = ship->shoot(gunCannons[i].x, gunCannons[i].y);
                            bullets.push_back(b);
                        }
                        this->soundBoard.find("laser")->second.play();
                    }
                }

                sf::Event event;
                while (window.pollEvent(event)) {

                    switch (event.type) {

                        case sf::Event::Closed:
                            window.close();
                            break;

                        case sf::Event::LostFocus:
                            afk_b = true;
                            break;

                        case sf::Event::GainedFocus:
                            afk_h ?: afk_b = false;
                            break;

                        case sf::Event::KeyPressed:
                            
                            switch (event.key.code) {

                                case sf::Keyboard::C:
                                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
                                        window.close();
                                    }
                                    break;

                                case sf::Keyboard::Escape:
                                    afk_b = !afk_b;
                                    afk_h = !afk_h;
                                    break;

                                default:
                                    break;

                            }

                        default:
                            break;

                    }

                }

                if (enemies_easy.size() < phase / 100 && enemies_easy.size() < enemiesEasyMax) {
                    enemies_easy.push_back(enemy_easy_c(&window, sf::Text(ascii_enemy_easy, fontShip, fontShipSize)));
                }

                ship->update();
                panel.update(ship->health, ship->cannonHeat);
                speedBonus = 9 - (ship->position.y > 0 ? ship->position.y : 0) * 10 / worldHeight;

                window.clear(sf::Color(bg.r,bg.g,bg.b));

                for (int i = 0; i < starDensity; ++i) {
                    starPosBuffer = stars[i].getGlobalBounds();
                    if (starPosBuffer.top > worldHeight) {
                        stars[i].setCharacterSize(rand() % 15 + 5);
                        stars[i].setPosition((rand() % (desktop.width+1)), 0);
                    } else {
                        stars[i].setPosition(starPosBuffer.left, (starPosBuffer.top + stars[i].getCharacterSize() / 3) + speedBonus);
                    }
                    window.draw(stars[i]);
                }

                for (int i = 0; i < bullets.size(); ++i) {
                    bullets[i].sprite.setPosition(bullets[i].position.x, bullets[i].position.y);
                    window.draw(bullets[i].sprite);
                    bullets[i].move();
                    sf::FloatRect bnds = bullets[i].sprite.getGlobalBounds();
                    if (bullets[i].position.x - bnds.width > window.getSize().x || bullets[i].position.x + bnds.height < 0\
                        || bullets[i].position.y - bnds.height > window.getSize().y || bullets[i].position.y + bnds.height < 0) {
                        bullets.erase(bullets.begin() + i);
                    }
                }

                for (int i = 0; i < enemies_easy.size(); ++i) {
                    enemies_easy[i].sprite.setPosition(enemies_easy[i].position.x, enemies_easy[i].position.y);
                    window.draw(enemies_easy[i].sprite);
                    enemies_easy[i].move(speedBonus);
                    if (enemies_easy[i].position.y > (int)desktop.height) {
                        enemies_easy.erase(enemies_easy.begin() + i);
                    }
                    for (int b = 0; b < bullets.size(); ++b) {
                        if ( enemies_easy[i].isHit(bullets[b].sprite) ) {
                            enemies_easy[i].damage(bullets[b].dmg);
                            bullets.erase(bullets.begin() + b);
                            if ( enemies_easy[i].health <= 0 ) {
                                enemies_easy.erase(enemies_easy.begin() + i);
                                break;
                            }
                        }
                    }
                    if (enemies_easy[i].isHit(ship->sprite)) {
                        ship->health -= enemies_easy[i].hit;
                        enemies_easy.erase(enemies_easy.begin() + i);
                    }
                }

                window.draw(ship->sprite);

                for (int i = 0; i < panel.panels.size(); ++i) { // dynamic because i'll add more panels in the future probably
                    window.draw(panel.panels[i]);
                }

                if (showDebugInfo) {
                    sf::String debug = "ship->position.x  = " + std::to_string(ship->position.x)\
                                        + "\nship->position.y  = " + std::to_string(ship->position.y)\
                                        + "\nship->speed.x     = " + std::to_string(ship->speed.x)\
                                        + "\nship->speed.y     = " + std::to_string(ship->speed.y)\
                                        + "\nship->cannonHeat  = " + std::to_string((int)ship->cannonHeat)\
                                        + "\n\nspeedBonus   = " + std::to_string((int)speedBonus)\
                                        + "\nbullets.size()   = " + std::to_string(bullets.size())\
                                        + "\nenemies.size()   = " + std::to_string(enemies_easy.size())\
                                        + "\n\nphase            = " + std::to_string(phase);
                    dbg.setString(debug);
                    window.draw(dbg);
                }

                window.display();
                phase += phaseSpeed;
            }
        }

    public:

        world(uint16_t width = 0, uint16_t height = 0) {

            LOG([>] Setting default variables);

            this->speedBonus = 0;
            this->afk_b, this->afk_h = false;
            this->desktop = width == 0 || height == 0 ? sf::VideoMode::getFullscreenModes()[0] : sf::VideoMode(width, height);
            this->window.create(desktop, "Lander II", ( width == 0 || height == 0 ? sf::Style::Fullscreen : sf::Style::None ) );
            this->window.setMouseCursorVisible(false);
            this->window.setFramerateLimit(60);
            this->worldHeight = desktop.height - panelHeight;
            this->phase = 0;
            this->panel.load(worldHeight, desktop.width, panelHeight);

            sf::SoundBuffer laser;

            LOG([>] Loading files);
            
            if (!this->fontStar.loadFromFile(fontStarPath) || !this->fontShip.loadFromFile(fontShipPath)) {

                LOG([X] Could not load font files);

            } else if (!this->startMusic.openFromFile(musicStartPath) || !this->bossEasyMusic.openFromFile(musicBossEasyPath)) {

                LOG([X] Could not load music files);

            } else if (!laser.loadFromFile(soundLaserPath)) {

                LOG([X] Could not load sound files);

            } else {

                this->startMusic.setLoop(true);
                this->bossEasyMusic.setLoop(true);

                this->soundBoard["laser"] = sf::Sound(laser);

                this->startMusic.play();

                LOG([>] Loading ship);
                std::wifstream in((std::string)"sprites/" + asciiSkin + "ship", std::ios::binary);
                in.imbue(std::locale(in.getloc(), new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>));
                std::wstring line;

                wchar_t c;
                while (in.get(c)) {
                    this->ascii_ship += c;
                }

                this->ship = new ship_c(&window, sf::Text(ascii_ship, fontShip, fontShipSize), sf::Text("|", fontShip, 25));

                LOG([>] Loading enemy_easy);
                in.close();
                line.clear();
                in.open((std::string)"sprites/" + asciiSkin + "enemy_easy");
                while (std::getline(in, line)) {
                    this->ascii_enemy_easy += line;
                    this->ascii_enemy_easy += '\n';
                }

                LOG([>] Supernovas in process);
                for (int i = 0; i < starDensity; ++i) {
                    sf::Text s("*", fontStar, (rand() % 15 + 5));
                    s.setFillColor(sf::Color(starColor.r, starColor.g, starColor.b, starColor.a));
                    s.setPosition((rand() % (desktop.width+1)), (rand() % (worldHeight+1)));
                    this->stars.push_back(s);
                }

                this->dbg = sf::Text("Debug", fontShip, 15);
                this->dbg.setPosition(15, 15);

                LOG([>] Starting main game loop);
                gameLoop();

            }

        }

};
