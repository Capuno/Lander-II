//#include <math.h> // sin, cos
//#define PI 3.14159265

#define GUN_SLOW    10
#define GUN_NORMAL  25
#define GUN_FAST    50

class baseEntity {

    protected:

        struct pos {
            int x, y;
        };

        struct spd {
            int8_t x, y = 0;
        };

    public:

        sf::RenderWindow* window;
        sf::FloatRect bounds;
        int8_t health = 100;
        sf::Text sprite;
        pos position;
        uint8_t gun;
        spd speed;

        void damage(uint8_t rng) {
        	uint8_t rng_e = rng * 2;
            this->health -= ( rand() % (++rng_e) + rng );
        }

        bool isHit(sf::Text collider) {
            return this->sprite.getGlobalBounds().intersects(collider.getGlobalBounds());
        }
};

class baseEnemy: public baseEntity {

    private:

        float xm, ym;

    protected:

        int angle = 180;

    public:

        void move(uint8_t sb) {
            this->position.x += this->speed.x + (sb * this->xm);
            this->position.y += this->speed.y + (sb * this->ym);
            if (this->position.x <= 0 || this->position.x >= this->window->getSize().x + this->bounds.width) {
                this->speed.x = -this->speed.x;
                this->sprite.setRotation(-this->sprite.getRotation());
            }
        }

        void spawn(uint8_t speed) {
            this->position.y = -this->sprite.getGlobalBounds().height;
            this->position.x = ( rand()  %  this->window->getSize().x );

            if ((rand() % 100 ) < 10) {
                int angleOffset = (rand() % (maxEnemyAngleOffset*2)) - maxEnemyAngleOffset;
                this->angle = angleOffset > 0 ? 180 - angleOffset : -180 - angleOffset;
            }
            this->xm = angle > 0 ? ((180 - angle) * 100 / 90 * .01) : -((angle * 100 / 90 * .01) + 2);
            this->ym = xm > 0 ? 1 - xm : 1 + xm;

            this->sprite.setRotation(angle);
            this->speed.y = speed * ym * 4;
            this->speed.x = speed * xm * 4;
        }
    
};

class bullet_c: public baseEntity {

    private:

        float xm, ym;

    public:

    	uint8_t dmg;
        
        void move() {
            this->position.x += this->speed.x;
            this->position.y += this->speed.y;
        }

        bullet_c(sf::RenderWindow* window, sf::Text sprite, uint8_t bt, float angle, unsigned int x, unsigned int y) {
            // TODO: properly use sin and cos for this instead of this lobotomy
            if (angle <= 90 && angle >= -90) {
                xm = angle * 100 / 90 * .01;
                ym = xm > 0 ? -(1 - xm) : -(1 + xm);
            } else if (angle <= 180 && angle >= -180) {
                xm = angle > 0 ? ((180 - angle) * 100 / 90 * .01) : -((angle * 100 / 90 * .01) + 2);
                ym = xm > 0 ? 1 - xm : 1 + xm;
            }
            this->window = window;
            this->speed.y = bt * ym;
            this->speed.x = bt * xm;
            this->dmg = bt * 2;
            this->sprite = sprite;
            this->sprite.setFillColor(sf::Color(playerShootColor.r, playerShootColor.g, playerShootColor.b, playerShootColor.a));
            this->sprite.setRotation(angle);
            this->position.x = x - sprite.getGlobalBounds().width;
            this->position.y = y - sprite.getGlobalBounds().height;
        }

};

class ship_c: public baseEntity {

    private:

        uint8_t sc;
        sf::Text bullet;
        struct color ssc;

    public:

        bool canShoot;
        uint16_t cannonHeat;

        ship_c(sf::RenderWindow* window, sf::Text sprite, sf::Text bulletSprite) {
            this->window = window;
            this->sprite = sprite;
            this->bullet = bulletSprite;
            this->bounds = sprite.getGlobalBounds();
            //this->bounds.width -= sprite.getCharacterSize() / 100;
            this->bounds.height += 5; // Do not ask
            sf::Vector2u wbnds = window->getSize();
            this->position.x = wbnds.x / 2 - this->bounds.width / 2;
            this->position.y = wbnds.y * .9 - this->bounds.height / 2;
            this->speed.x, this->speed.y = 0;
            this->health = 100;
            this->gun = GUN_NORMAL;
            this->cannonHeat = 0;
            this->sc = 0;
            this->sprite.setOutlineColor(sf::Color(bg.r,bg.g,bg.b));
            this->sprite.setOutlineThickness(6);
        }

        void update() {
            this->sc <= 0 ?: --this->sc;
            this->cannonHeat -= this->cannonHeat <= 0 ? 0 : shootJouleCooldown;
            this->canShoot = cannonHeat + shootJoule < 1000 && sc <= 0;
            this->ssc = this->health >= 66 ? fontShipColorFull : ( this->health >= 33 ? fontShipColorHalf : fontShipColorDanger );
            this->position.x += this->speed.x;
            this->position.y += this->speed.y;
            this->sprite.setPosition(this->position.x, this->position.y);
            this->sprite.setFillColor(sf::Color(ssc.r, ssc.g, ssc.b));
        }

        bullet_c shoot(uint8_t x, uint8_t y) {
            if (canShoot) {
                sf::FloatRect bnds = sprite.getGlobalBounds();
                this->cannonHeat += shootJoule;
                this->sc = shootCooldown;
                x = x > this->bounds.width ? this->bounds.width : x;
                y = y > this->bounds.height ? this->bounds.height : y;
                return bullet_c(this->window, this->bullet, this->gun, 0, bnds.left + x, bnds.top + y);
            }
        }

};

class enemy_easy_c: public baseEnemy{

    public:

        uint8_t hit = 10;

        enemy_easy_c(sf::RenderWindow* window, sf::Text sprite) {
            this->window = window;
            this->sprite = sprite;
            this->sprite.setFillColor(sf::Color(enemyEasyColor.r, enemyEasyColor.g, enemyEasyColor.b, enemyEasyColor.a));
            this->bounds = sprite.getGlobalBounds();
            spawn(2);
        }

};
