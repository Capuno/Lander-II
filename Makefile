run:
	g++ main.cpp -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -no-pie -s -o build/lander

debug:
	g++ main.cpp -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -no-pie -g3 -s -o build/lander