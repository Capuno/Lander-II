#include <sstream>

struct color {
    uint8_t r, g, b, a = 255;
};

const struct color bg = { 33, 29, 27 };
const struct color fg = { 48, 43, 40 };
const struct color sg = { 64, 56, 52 };
const struct color tc = { 161, 161, 161 };
const struct color fontShipColorFull = { 130, 255, 70 };
const struct color fontShipColorHalf = { 255, 130, 70 };
const struct color fontShipColorDanger = { 255, 70, 70 };
const struct color starColor = { 255, 255, 255, 90 };

const struct color playerShootColor = { 200, 0, 255};

const struct color panelBase = { 48, 43, 40 };
const struct color panelEmpty = { 33, 29, 27 };
const struct color panelFill = { 100, 95, 90 };

const struct color enemyEasyColor = { 255, 50, 50 };

// [menu]
bool menuEnabled;
std::string fontBasePath;
std::string fontTitlePath;
std::string musicMenuPath;
std::string soundBtnPath;
std::string soundAFKFocusPath;
std::string soundAFKBlurPath;
std::string artAfkPath;
bool tips;

// [game]
bool showDebugInfo;
std::string musicStartPath;
std::string musicBossEasyPath;
unsigned int starDensity;
std::string fontStarPath;
unsigned int maxEnemyAngleOffset;
unsigned int panelHeight;
uint8_t phaseSpeed;
std::string asciiSkin;
uint16_t enemiesEasyMax;

// [ship]
std::string soundLaserPath;
std::string fontShipPath;
unsigned int fontShipSize;
uint8_t shipAcceleration;
uint8_t shipStartSpeed;
uint8_t shipMaxSpeed;

// [guns]
unsigned int shootCooldown;
uint8_t shootJoule;
uint8_t shootJouleCooldown;
uint8_t totalGuns;
std::vector<sf::Vector2<uint8_t>> gunCannons;

// TODO sounds also in theme?

class settings {

    private:

        std::string rawIni;

    public:

        template <class dt>
        dt get(const char* block, const char* value) {
            dt data;
            std::stringstream rawstream(this->rawIni.c_str());
            std::stringstream block2e;
            block2e << rawstream.str().substr(rawstream.str().find(std::string(std::string("[")+std::string(block)+std::string("]")))+1);
            std::string line, buffer;
            while(std::getline(block2e,line,'\n') && line[0] != '['){
                if (line.substr(0, std::string(value).length()+1) == std::string(value)+std::string("=")) {
                    int d = std::string(value).length()+1;
                    line = line.substr(std::string(value).length()+1, line.find(';') != std::string::npos ? (int)line.find(';') - d : line.length());
                    buffer = line.erase(line.find_last_not_of(" \t")+1);
                    break;
                }
            }
            if (!buffer.empty()) {
                std::istringstream ss(buffer);
                ss >> data;
            }

            return data;
        }

        // TODO
        //color getColor();

        settings(const char* iniPath) {
            std::stringstream packed;
            std::string line;
            std::ifstream in(iniPath, std::ios::in | std::ios::binary);
            std::stringstream rawstream(std::string((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>()).c_str());
            while(std::getline(rawstream,line,'\n')){
                if (!line.empty()) {
                    line.erase(0, line.find_first_not_of(" \t"));
                    line = line.substr(0, line.find(';') != std::string::npos ? (int)line.find(';') : line.length());
                    int bep = line.find(" = ");
                    if (bep != std::string::npos) {
                        line.erase(bep, 3);
                        line.insert(bep, "=");
                    }
                    packed << line.erase(line.find_last_not_of(" \t")+1) + "\n";
                }
            }
            for (int i = 0; i < packed.str().size(); ++i) {
                if (packed.str()[i] != '"') {
                    this->rawIni += packed.str()[i];
                }
            }
        }

};
