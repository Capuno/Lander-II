#include <iostream>
#include <string>
#include <fstream>
#include <ctime>

#include <locale>
//#include <iostream>

#include <codecvt>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#define LOG(X) printf(__FILE__":%d: "#X"\n", __LINE__)

#include "settings.hpp"
#include "lander.cpp"

int menu() {

    //sf::ContextSettings aa;
    //aa.antialiasingLevel = 20;

    sf::RenderWindow menu(sf::VideoMode(600, 300), "Lander Menu", sf::Style::None);
    //menu.setVerticalSyncEnabled(true);
    menu.setFramerateLimit(60);

    sf::Font fontBase, fontTitle;
    sf::SoundBuffer btnSoundBuffer, AFKBlurBuffer, AFKFocusBuffer;
    sf::Music bgMusic;
    if (!fontBase.loadFromFile(fontBasePath) || !fontTitle.loadFromFile(fontTitlePath) || !bgMusic.openFromFile(musicMenuPath) \
        || !btnSoundBuffer.loadFromFile(soundBtnPath) || !AFKBlurBuffer.loadFromFile(soundAFKBlurPath) || !AFKFocusBuffer.loadFromFile(soundAFKFocusPath)) {
        LOG(Could not load files);
        return 1;
    }
    sf::Sound btnSound, AFKBlur, AFKFocus;
    btnSound.setBuffer(btnSoundBuffer);
    AFKBlur.setBuffer(AFKBlurBuffer);
    AFKFocus.setBuffer(AFKFocusBuffer);
    bgMusic.setLoop(true);
    bgMusic.play();

    sf::Text tipMouse("Forget about your mouse, this game is keyboard only!\t|\tYou can change the sprites simply editing the ascii art files.\
        \t|\tThis game is open source.\t|\tPress Ctrl+C to stop playing anytime.\t|\tYou can change almost everything from the config file.\
        \t|\tDon't like a song or sound? You can change it!", fontBase, 15);
    tipMouse.setFillColor(sf::Color(tc.r, tc.g, tc.b));
    tipMouse.setPosition(610, 10);

    sf::Text tipControls("[<] Select Left | [>] Select Right | [Return] Open Option", fontBase, 17);
    tipControls.setFillColor(sf::Color(tc.r, tc.g, tc.b));
    tipControls.setPosition(40, 250);

    std::string art_afk, line;
    std::ifstream in(artAfkPath);
    while(std::getline(in, line)) {
        art_afk += line + "\n";
    }
    sf::Text afk_art(art_afk.c_str(), fontBase, 15);
    afk_art.setPosition(300 - afk_art.getGlobalBounds().width / 2, 150 - afk_art.getGlobalBounds().height / 2);
    sf::RectangleShape afk_bg(sf::Vector2f(600,300));
    afk_bg.setFillColor(sf::Color(0,10,10,200));
    bool afk_b = false;

    sf::ConvexShape menuBtn[3];
    sf::Text menuTtl[3];

    menuTtl[0].setString("PLAY");
    menuTtl[0].setFont(fontTitle);
    menuTtl[0].setCharacterSize(40);
    menuTtl[0].setPosition(85, 120);
    menuBtn[0].setPointCount(4);
    menuBtn[0].setPoint(0, sf::Vector2f(0, 0));
    menuBtn[0].setPoint(1, sf::Vector2f(150, 0));
    menuBtn[0].setPoint(2, sf::Vector2f(120, 100));
    menuBtn[0].setPoint(3, sf::Vector2f(0, 100));
    menuBtn[0].setPosition(50, 100);

    menuTtl[1].setString("SETTINGS");
    menuTtl[1].setFont(fontTitle);
    menuTtl[1].setCharacterSize(40);
    menuTtl[1].setPosition(220, 120);
    menuBtn[1].setPointCount(4);
    menuBtn[1].setPoint(0, sf::Vector2f(0, 0));
    menuBtn[1].setPoint(1, sf::Vector2f(190, 0));
    menuBtn[1].setPoint(2, sf::Vector2f(160, 100));
    menuBtn[1].setPoint(3, sf::Vector2f(-30, 100));
    menuBtn[1].setPosition(205, 100);

    menuTtl[2].setString("QUIT");
    menuTtl[2].setFont(fontTitle);
    menuTtl[2].setCharacterSize(40);
    menuTtl[2].setPosition(430, 125);
    menuBtn[2].setPointCount(4);
    menuBtn[2].setPoint(0, sf::Vector2f(0, 0));
    menuBtn[2].setPoint(1, sf::Vector2f(150, 0));
    menuBtn[2].setPoint(2, sf::Vector2f(150, 100));
    menuBtn[2].setPoint(3, sf::Vector2f(-30, 100));
    menuBtn[2].setPosition(400, 100);

    int menuBtnSelected = 1;

    while (menu.isOpen()) {
        sf::Event event;
        while (menu.pollEvent(event)) {

            switch (event.type) {

                case sf::Event::Closed:
                    menu.close();
                    return 0;

                case sf::Event::LostFocus:
                    // Remember Animal Crossing for the ds? the sound it made when you HLTd the ds
                    // 2006, what a time to be alive
                    AFKFocus.stop();
                    AFKBlur.play();
                    bgMusic.pause();
                    afk_b = true;
                    break;

                case sf::Event::GainedFocus:
                    // remove HLTd effect
                    AFKBlur.stop();
                    AFKFocus.play();
                    bgMusic.play();
                    afk_b = false;
                    break;

                case sf::Event::KeyPressed:
                    
                    switch (event.key.code) {
                        case sf::Keyboard::C:
                            if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
                                menu.close();
                                return 0;
                            }
                            break;
                        case sf::Keyboard::Escape:
                            menu.close();
                            return 0;
                        case sf::Keyboard::Left:
                            btnSound.play();
                            menuBtnSelected <= 0 ?: menuBtnSelected--;
                            break;
                        case sf::Keyboard::Right:
                            btnSound.play();
                            menuBtnSelected >= 2 ?: menuBtnSelected++;
                            break;
                        case sf::Keyboard::Return:
                            menu.close();
                            return 2 + menuBtnSelected;
                        default:
                            break;
                    }

                default:
                    break;

            }

        }

        menu.clear(sf::Color(bg.r,bg.g,bg.b));

        if (tips) {
            menu.draw(tipControls);
            menu.draw(tipMouse);
            tipMouse.move(-1, 0);
            if (tipMouse.getPosition().x < -(tipMouse.getGlobalBounds().width+100)) {
                tipMouse.setPosition(610, 10);
            }
        }

        for (int i = 0; i < 3; ++i) {
            if (i == menuBtnSelected) {
                menuBtn[i].move(3, 3);
                menuBtn[i].setFillColor(sf::Color(0, 0, 0, 100));
                menu.draw(menuBtn[i]);
                menuBtn[i].move(-3, -3);
                menuBtn[i].setFillColor(sf::Color(sg.r, sg.g, sg.b));
                menu.draw(menuBtn[i]);
                menuTtl[i].setFillColor(sf::Color(0, 0, 0, 100));
                menuTtl[i].move(3, 3);
                menu.draw(menuTtl[i]);
                menuTtl[i].setFillColor(sf::Color(tc.r, tc.g, tc.b));
                menuTtl[i].move(-3, -3);
                menu.draw(menuTtl[i]);
            } else {
                menuBtn[i].setFillColor(sf::Color(fg.r, fg.g, fg.b));
                menu.draw(menuBtn[i]);
                menuTtl[i].setFillColor(sf::Color(tc.r, tc.g, tc.b));
                menu.draw(menuTtl[i]);
            }
        }

        if (afk_b) {
            menu.draw(afk_bg);
            menu.draw(afk_art);
        }

        menu.display();

    }

}

int main(int argc, char const *argv[]) {

    // TODO ASCII art here just for the lolz

    srand(time(NULL));
    setlocale(LC_CTYPE, "");

    settings cfg("settings.ini"); // argv?

    LOG([>] Loading menu configuration);
    menuEnabled = cfg.get<bool>("menu", "menuEnabled");
    fontBasePath = cfg.get<std::string>("menu", "fontBasePath");
    fontTitlePath = cfg.get<std::string>("menu", "fontTitlePath");
    musicMenuPath = cfg.get<std::string>("menu", "musicMenuPath");
    soundBtnPath = cfg.get<std::string>("menu", "soundBtnPath");
    soundAFKFocusPath = cfg.get<std::string>("menu", "soundAFKFocusPath");
    soundAFKBlurPath = cfg.get<std::string>("menu", "soundAFKBlurPath");
    artAfkPath = cfg.get<std::string>("menu", "artAfkPath");
    tips = cfg.get<bool>("menu", "tips");

    int ec = menuEnabled ? menu() : 2;

    switch(ec) {

        case 0:
        case 1:
        case 4:
            break;

        case 2:

            LOG([>] Loading game configuration);
            showDebugInfo = cfg.get<bool>("game", "showDebugInfo");
            musicStartPath = cfg.get<std::string>("game", "musicStartPath");
            musicBossEasyPath = cfg.get<std::string>("game", "musicBossEasyPath");
            starDensity = cfg.get<unsigned int>("game", "starDensity");
            fontStarPath = cfg.get<std::string>("game", "fontStarPath");
            maxEnemyAngleOffset = cfg.get<int>("game", "maxEnemyAngleOffset");
            panelHeight = cfg.get<unsigned int>("game", "panelHeight");
            phaseSpeed = cfg.get<int>("game", "phaseSpeed");
            enemiesEasyMax = cfg.get<int>("game", "enemiesEasyMax");
            asciiSkin = cfg.get<std::string>("game", "asciiSkin");
            asciiSkin = asciiSkin == "default" ? "" : asciiSkin + "/";

            LOG([>] Loading ship configuration);
            fontShipPath = cfg.get<std::string>("ship", "fontShipPath");
            fontShipSize = cfg.get<unsigned int>("ship", "fontShipSize");
            shipAcceleration = cfg.get<unsigned int>("ship", "shipAcceleration");
            shipStartSpeed = cfg.get<unsigned int>("ship", "shipStartSpeed");
            shipMaxSpeed = cfg.get<unsigned int>("ship", "shipMaxSpeed");
            soundLaserPath = cfg.get<std::string>("ship", "soundLaserPath");

            LOG([>] Loading guns configuration);
            shootCooldown = cfg.get<unsigned int>("guns", "shootCooldown");
            shootJoule = cfg.get<int>("guns", "shootJoule");
            shootJouleCooldown = cfg.get<int>("guns", "shootJouleCooldown");
            totalGuns = cfg.get<int>("guns", "totalGuns");

            for (int i = 1; i <= (int)totalGuns; ++i) {
                sf::Vector2<uint8_t> cannon(cfg.get<int>("guns", ((std::string)"gun-" + std::to_string(i) + "-OffsetX").c_str()),
                                            cfg.get<int>("guns", ((std::string)"gun-" + std::to_string(i) + "-OffsetX").c_str()));
                gunCannons.push_back(cannon);
            }

            world(1024, 768);

        case 3:
            //settings();
            // xdg-open settings.ini / UI ?
        default:
            break;
    }
    return 0;
}